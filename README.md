# dockerfiles

Dockerfiles for building containers with systemd running.
All containers should be run with `--privileged` option for systemd support.

## systemd
Contains Dockerfile for a base image

## vagrant

- use `latest` tag for pure systemd + ssh + vagrant bits
- use `puppet3`, `puppet4` or `puppet5` for vagrant enabled containers with puppet and librarian-puppet installed
